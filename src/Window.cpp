
// Project
#include "Window.h"
#include <iostream>

#include <stdio.h>

#include <QDebug>
#include <QKeyEvent>
#include <QString>
#include <QString>


#include "Scene.h"

void Window::printVersionInformation()
{
    //// uncomment to the following line to reproduce the linker error (cmake)
//    glGetString(GL_VERSION);
}


Window::Window(QWindow *parent) : QOpenGLWindow(NoPartialUpdate, parent)
{
  //connect(&m_timer, SIGNAL(timeout()), this, SLOT(update()));

  qDebug()<< format().swapInterval();

  if(format().swapInterval() == -1)
  {
      // V_blank synchronization not available (tearing likely to happen)
      qDebug("Swap Buffers at v_blank not available: refresh at approx 60fps.");
      m_timer.setInterval(17);
  }
  else
  {
      // V_blank synchronization available
      m_timer.setInterval(0);
  }

  m_timer.start();

}

Scene *Window::scene() const
{
  return m_scene;
}

void Window::setScene(Scene *_scene)
{
  m_scene = _scene;
}

void Window::initializeGL()
{
  // Init OpenGL Backend  (QOpenGLFunctions)
  if (scene())
    scene()->initialize();

  printVersionInformation();

  connect(context(), SIGNAL(aboutToBeDestroyed()), this, SLOT(teardownGL()), Qt::DirectConnection);
  connect(this, SIGNAL(frameSwapped()), this, SLOT(update()));
}

void Window::paintGL()
{
  if (scene())
    scene()->paint();
}

void Window::resizeGL(int _w, int _h)
{
  qDebug("New window size: %d, %d", _w, _h);
}

void Window::teardownGL()
{
    /*
  // Actually destroy our OpenGL information
  m_object.destroy();
  m_vertex.destroy();
  delete m_program;
  */
}

void Window::update()
{
    inputManager::update();
    // handle key press events

    if(inputManager::keyPressed(Qt::Key_Up))
    {
        qDebug(" -forward");
        scene()->m_myTransform.translate(0.0, 0.03, 0.0);
    }
    if(inputManager::keyPressed(Qt::Key_Down))
    {
        qDebug(" -down");
        scene()->m_myTransform.translate(0.0, -0.03, 0.0);
    }
    if(inputManager::keyPressed(Qt::Key_Left))
    {
        qDebug(" -left");
        scene()->m_myTransform.translate(0.03, 0.0, 0.0);
    }
    if(inputManager::keyPressed(Qt::Key_Right))
    {
        qDebug(" -right");
        scene()->m_myTransform.translate(-0.03, 0.0, 0.0);
    }

    scene()->update();
    QOpenGLWindow::update();
}

void Window::keyPressEvent(QKeyEvent *event)
{

    if (event->isAutoRepeat())
    {
      event->ignore();
    }
    else
    {
      inputManager::registerKeyPress(event->key());
    }
}

void Window::keyReleaseEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat())
    {
      event->ignore();
    }
    else
    {
      inputManager::registerKeyRelease(event->key());
    }
}

